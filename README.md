Used python version: [3.7.0](https://www.python.org/ftp/python/3.7.0/python-3.7.0.exe)

All dependencies can be installed using `pip install -r requirements.txt`

For configuring endpoint check `config/config.ini`

Reference TCs: [google docs](https://docs.google.com/spreadsheets/d/1QCwf-FSEsIuF2S5x59kcJm1eElDczgoj45Gc2-Lbq_o/edit?usp=sharing)
