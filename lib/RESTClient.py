"""Library providing keywords for robot tests"""
import os
import re
import typing
from configparser import RawConfigParser
from dataclasses import dataclass

import requests
from robot.api.deco import keyword
from robot.libraries.BuiltIn import BuiltIn
from robot.libraries.String import String

__version__ = "0.1"

CONFIG_PATH = f"{os.path.dirname(__file__)}/../config/config.ini"


class UserMapping(typing.NamedTuple):
    """User data storage"""

    id: str
    first_name: str
    last_name: str

    def __repr__(self):
        return f"UserMapping(id={self.id}, first_name='{self.first_name}', last_name='{self.last_name}')"


def default_getter(response: requests.Response) -> typing.Any:
    """Default data getter for :class:`ParsedResponse`"""
    return response.text


@dataclass
class ParsedResponse:
    """Container for `status code + data` pair"""
    status_code: int
    data: typing.Any

    def __iter__(self):
        """Implement it so it can be assigned to 2 variables, like ``x, y = ParsedResponse(...)``"""
        yield self.status_code
        yield self.data

    def __init__(self, response: requests.Response, data_getter=default_getter):
        """Converts :class:`requests.Response` object to detailed container

        :param response: Source response
        :param data_getter: Method to store data from request in container
        """
        self.status_code = response.status_code
        self.data = data_getter(response)


class RESTClient:
    """Test library kw storage"""

    ROBOT_LIBRARY_SCOPE = "GLOBAL"
    ROBOT_LIBRARY_VERSION = __version__

    builtin = BuiltIn()
    string = String()

    def __init__(self):
        self.session = requests.session()
        self.session.trust_env = False  # ignore proxies if set in environment
        self.session.headers["Accept"] = "application/json"  # useless, server just ignores
        config = RawConfigParser()
        config.read(CONFIG_PATH)
        self.base_url: str = config.get("GENERAL", "base_url")
        self.users_url: str = f"{self.base_url}/users"

    @keyword(name="List Users")
    def get_all_users(self):
        """Get existing user list"""
        response = self.session.get(self.users_url)
        response = ParsedResponse(response, parse_users)
        self.builtin.log(f"All Users: {response.data}")
        return response

    @keyword
    def find_user(self, user_id) -> ParsedResponse:
        """Find user in users list and return it"""
        if isinstance(user_id, UserMapping):
            user_id = user_id.id
        response = ParsedResponse(self.session.get(f"{self.users_url}/{user_id}"), parse_single_user)
        self.builtin.log(f"Single User: {response.data}")
        return response

    @keyword
    def create_new_user(self, first_name=None, last_name=None):
        """Send create user request, returning new user created"""
        if first_name is None:
            first_name = self.string.generate_random_string(15, chars="[LETTERS] ").strip()
        if last_name is None:
            last_name = self.string.generate_random_string(15, chars="[LETTERS] ").strip()
        response = self.session.post(self.users_url, headers=name_headers(first_name, last_name))
        return ParsedResponse(response, parse_single_user)

    @keyword
    def update_user(self, user: UserMapping, first_name=None, last_name=None):
        """Update given user fields"""
        first_name = user.first_name if first_name is None else first_name
        last_name = user.last_name if last_name is None else last_name
        response = self.session.put(f"{self.users_url}/{user.id}", headers=name_headers(first_name, last_name))
        return ParsedResponse(response)

    @keyword
    def delete_user(self, user: typing.Union[UserMapping, str, int]):
        """Remove user from user list"""
        user_id = user if isinstance(user, (str, int)) else user.id
        response = self.session.delete(f"{self.users_url}/{user_id}")
        return ParsedResponse(response)

    @keyword
    def user_should_exist(self, user_id) -> None:
        """Check if user with given ID exists"""
        if isinstance(user_id, UserMapping):
            user_id = user_id.id
        response = self.find_user(user_id)
        assert response.status_code == 200, \
            f"Invalid status code ({response.status_code}) searching for user with id {user_id}"
        existing_user = response.data
        assert existing_user is not None, f"No user exists with id {user_id}"

    @keyword
    def user_should_not_exist(self, user_id):
        """Check if user with given ID does not exist"""
        if isinstance(user_id, UserMapping):
            user_id = user_id.id
        response = self.find_user(user_id)
        assert response.status_code == 200, \
            f"Invalid status code ({response.status_code}) searching for user with id {user_id}"
        existing_user = response.data
        assert existing_user is None, f"User exists with id {user_id}"

    @keyword
    def user_should_be(self, expected: UserMapping):
        """Validates that given user exist on server with same data"""
        self.user_should_exist(expected.id)
        existing = self.find_user(expected.id).data
        assert existing == expected, \
            f"User in database is not the same as given (expected `{expected}`, found `{existing}`)"

    @staticmethod
    @keyword("Status Code Should Be")
    def eq_codes(actual_code, expected_code):
        """Validates if status code is expected one"""
        if int(actual_code) != int(expected_code):
            raise AssertionError(f"Response with code `{actual_code}` is received, expected is `{expected_code}`")

    @staticmethod
    @keyword
    def user(user_id, last_name="", first_name=""):
        """Returns user container"""
        return UserMapping(user_id, first_name, last_name)

    @keyword
    def add_header(self, key, value):
        """Append header to session"""
        self.session.headers.update({key: value})

    @keyword
    def remove_header(self, key):
        """Remove header from session, if exists"""
        try:
            self.session.headers.pop(key)
        except KeyError:
            pass


def parse_users(raw_response: requests.Response):
    """Convert server raw response to list/dictionary"""
    res = re.findall(r"{ID=(\d+), FIRSTNAME=(.*?), LASTNAME=(.*?)}", raw_response.text)
    users = [UserMapping(*items) for items in res]
    return users


def parse_single_user(raw_response: requests.Response):
    """Parse server response to get single user"""
    users = parse_users(raw_response)
    if not users:
        return None
    return users[0]


def name_headers(first_name=None, last_name=None, user_id=None):
    """Return headers dictionary with given data"""
    headers = {}
    if first_name != "":
        headers.update({"firstName": first_name})
    if last_name != "":
        headers.update({"lastName": last_name})
    if user_id is not None:
        headers.update({"userId": user_id})
    return headers
