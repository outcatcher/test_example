*** Settings ***
Test Setup        Create Test User And Remember
Test Teardown     Remove Test User
Library           lib.RESTClient
Library           String

*** Variables ***
${test user}      ${NONE}
${punctuation}    ~`!@#$%^&*()][;',./\<>?

*** Test Cases ***
Create User
    [Documentation]    Create new user and check for it in the list
    [Setup]
    ${status code}    ${test user}=    Create New User
    Status Code Should Be    ${status code}    200
    ${status code}    ${user list}=    List Users
    Status Code Should Be    ${status code}    200
    Should Contain    ${user list}    ${test user}
    [Teardown]    Run Keyword And Ignore Error    Delete User    ${test user}

Delete Existing User
    ${status code}    ${_}=    Delete User    ${test user}
    Status Code Should Be    ${status code}    200
    ${status code}    ${user list}=    List Users
    Should Not Contain    ${user list}    ${test user}
    User Should Not Exist    ${test user.id}
    [Teardown]

Find Existing User
    ${status code}    ${user}=    Find User    ${test user.id}
    Status Code Should Be    ${status code}    200
    Should Be Equal    ${user}    ${test user}

Create User. Anonymous
    [Documentation]    Create User without first and last name
    [Setup]
    ${status code}    ${test user}=    Create New User    ${EMPTY}    ${EMPTY}
    User Should Exist    ${test user.id}
    [Teardown]    Run Keyword And Ignore Error    Delete User    ${test user}

Create User. ID Header
    [Documentation]    Check that userId header is not considered during creation
    [Setup]
    ${id}=    Generate Random String    10    [NUMBERS]
    Add Header    userId    ${id}
    ${status code}    ${test user}=    Create New User
    Remove Header    userId
    Should Not Be Equal As Integers    ${test user.id}    ${id}
    User Should Not Exist    ${id}
    [Teardown]    Run Keyword And Ignore Error    Delete User    ${test user}

Create User With Strange Symbols
    [Tags]    known issue
    [Setup]
    ${first name}=    Generate Random String    50    chars=${punctuation}
    ${last name}=    Generate Random String    50    chars=${punctuation}
    ${status code}    ${test user}=    Create New User    ${first name}    ${last name}
    Status Code Should Be    ${status code}    200
    User Should Exist    ${test user.id}
    [Teardown]    Run Keyword And Ignore Error    Delete User    ${test user}

Create User With Long Name
    [Setup]
    ${first name}=    Generate Random String    150    [LETTERS] \
    ${last name}=    Generate Random String    150    [LETTERS] \
    ${status code}    ${test user}=    Create New User    ${first name}    ${last name}
    Status Code Should Be    ${status code}    200
    User Should Exist    ${test user.id}
    [Teardown]    Run Keyword And Ignore Error    Delete User    ${test user}

Update Existing User. First Name
    ${first name}=    Generate Random String    10    [LETTERS] \
    ${status code}    ${_}=    Update User    ${test user}    first_name=${first name}
    Status Code Should Be    ${status code}    200
    ${status code}    ${updated user 1}=    Find User    ${test user.id}
    Status Code Should Be    ${status code}    200
    Should Be Equal    ${test user.last_name}    ${updated user 1.last_name}
    Should Not Be Equal    ${test user.first_name}    ${updated user 1.first_name}
    Should Be Equal    ${first name}    ${updated user 1.first_name}

Update Existing User. Last Name
    ${last name}=    Generate Random String    10    [LETTERS] \
    ${status code}    ${_}=    Update User    ${test user}    last_name=${last name}
    Status Code Should Be    ${status code}    200
    ${status code}    ${updated user 1}=    Find User    ${test user.id}
    Status Code Should Be    ${status code}    200
    Should Be Equal    ${test user.first_name}    ${updated user 1.first_name}
    Should Not Be Equal    ${test user.last_name}    ${updated user 1.last_name}
    Should Be Equal    ${last name}    ${updated user 1.last_name}

Update Existing User. Id
    [Documentation]    Test that ID can't be changed
    [Tags]    known issue
    ${id}=    Generate Random String    5    [NUMBERS]
    Add Header    userId    ${id}
    ${status code}    ${_}=    Update User    ${test user}
    Remove Header    userId
    Status Code Should Be    ${status code}    200    # this seems incorrect
    User Should Not Exist    ${id}
    User Should Exist    ${test user.id}

Update Existing User. Anonymous
    ${status code}    ${_}=    Update User    ${test user}    ${EMPTY}    ${EMPTY}
    Status Code Should Be    ${status code}    200
    ${status code}    ${updated user}=    Find User    ${test user.id}
    Status Code Should Be    ${status code}    200
    Should Be Equal    ${updated user.first_name}    ${EMPTY}
    Should Be Equal    ${updated user.last_name}    ${EMPTY}

Update Not Existing User
    [Tags]    known issue
    [Setup]
    ${id}=    Generate Random String    10    [NUMBERS]
    ${test user}=    User    ${id}
    ${status code}    ${_}=    Update User    ${test user}    ${EMPTY}    ${EMPTY}
    Status Code Should Be    ${status code}    404    # or some other 400+ status
    User Should Not Exist    ${id}

Delete Not Existing User
    [Tags]    known issue
    [Setup]
    ${id}=    Generate Random String    10    [NUMBERS]
    ${test user}=    User    ${id}
    ${status code}    ${_}=    Delete User    ${test user}
    Status Code Should Be    ${status code}    404    # or some other 400+ status
    [Teardown]

Update Another User
    [Documentation]    Check that user given in path is updated, not the one set in header
    ${_}    ${test user2}=    Create New User
    Add Header    userId    ${test user2.id}
    ${status code}    ${_}=    Update User    ${test user}    ${EMPTY}    ${EMPTY}
    Remove Header    userId
    ${status code}    ${updated user}=    Find User    ${test user}
    ${status code}    ${updated user2}=    Find User  `  ${test user2}
    Run Keyword And Continue On Failure    Should Be Equal    ${updated user.first_name}    ${EMPTY}
    Run Keyword And Continue On Failure    Should Be Equal    ${updated user.last_name}    ${EMPTY}
    Run Keyword And Continue On Failure    Should Be Equal    ${updated user2.first_name}    ${test user2.first_name}
    Run Keyword And Continue On Failure    Should Be Equal    ${updated user2.last_name}    ${test user2.last_name}
    [Teardown]    Remove Several Users    ${test user}    ${test user2}

Delete Another User
    [Documentation]    Check that user given in path is updated, not the one set in header
    ${_}    ${test user2}=    Create New User
    Add Header    userId    ${test user2.id}
    ${status code}    ${_}=    Delete User    ${test user}
    Remove Header    userId
    Run Keyword And Continue On Failure    User Should Not Exist    ${test user}
    Run Keyword And Continue On Failure    User Should Exist    ${test user2}
    [Teardown]    Run Keywords    Remove Header    userId
    ...    AND    Remove Several Users    ${test user}    ${test user2}

Find Another User
    [Documentation]    Check that user given in path is found, not the one set in header
    ${_}    ${test user2}=    Create New User
    Add Header    userId    ${test user2.id}
    ${_}    ${found user}=    Find User    ${test user}
    Remove Header    userId
    Run Keyword And Continue On Failure    Should Be Equal    ${found user}    ${test user}
    Run Keyword And Continue On Failure    Should Not Be Equal    ${found user}    ${test user2}
    [Teardown]    Run Keywords    Remove Header    userId
    ...    AND    Remove Several Users    ${test user}    ${test user2}

Find Not Existing User
    [Setup]
    ${id}=    Generate Random String    10    [NUMBERS]
    ${status code}    ${found user}=    Find User    ${id}
    Status Code Should Be    ${status code}    200
    Should Not Be True    ${found user}
    [Teardown]

Find Not Existing User. Non-numeric ID
    [Setup]
    ${id}=    Generate Random String    10    [LETTERS]${punctuation}
    ${status code}    ${found user}=    Find User    ${id}
    Status Code Should Be    ${status code}    200
    Should Not Be True    ${found user}
    [Teardown]

List Users. Several Users Exist
    ${_}    ${test user2}=    Create New User
    ${_}    ${user list}=    List Users
    Should Contain    ${user list}    ${test user}
    Should Contain    ${user list}    ${test user2}
    [Teardown]    Remove Several Users    ${test user}    ${test user2}

Delete Not Existing User. Non-numeric ID
    [Setup]
    ${id}=    Generate Random String    10    [LETTERS]${punctuation}
    ${status code}    ${_}=    Delete User    ${id}
    Status Code Should Be    ${status code}    200
    [Teardown]

*** Keywords ***
Create Test User And Remember
    [Documentation]    Creates new user and store it as Test Variable `${test user}`
    ${_}    ${test user}=    Create New User
    Set Test Variable    ${test user}

Remove Test User
    [Documentation]    Removes user defined in `${test user}` Test Variable
    Run Keyword And Ignore Error    Delete User    ${test user}

Remove Several Users
    [Arguments]    @{users}
    : FOR    ${user}    IN    @{users}
    \    Run Keyword And Ignore Error    Delete User    ${user}
