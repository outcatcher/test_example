*** Settings ***
Test Setup        Create Test User And Remember
Test Teardown     Remove Test User
Library           lib.RESTClient
Library           String

*** Variables ***
${test user}      ${NONE}

*** Test Cases ***
Create User
    [Documentation]    Create new user and check for it in the list
    [Setup]
    ${status code}    ${test user}=    Create New User
    Status Code Should Be    ${status code}    200
    ${status code}    ${user list}=    List Users
    Status Code Should Be    ${status code}    200
    Should Contain    ${user list}    ${test user}
    [Teardown]    Run Keyword And Ignore Error    Delete User    ${test user}

Delete Existing User
    [Setup]    Create Test User And Remember
    ${status code}    ${_}=    Delete User    ${test user}
    Status Code Should Be    ${status code}    200
    ${status code}    ${user list}=    List Users
    Should Not Contain    ${user list}    ${test user}
    User Should Not Exist    ${test user.id}
    [Teardown]

Find Existing User
    ${status code}    ${user}=    Find User    ${test user.id}
    Status Code Should Be    ${status code}    200
    Should Be Equal    ${user}    ${test user}

Create User. Anonymous
    [Documentation]    Create User without first and last name
    [Setup]
    ${status code}    ${test user}=    Create New User    ${EMPTY}    ${EMPTY}
    User Should Exist    ${test user.id}
    [Teardown]    Run Keyword And Ignore Error    Delete User    ${test user}

Create User With Strange Symbols
    [Tags]    known issue
    [Setup]
    ${first name}=    Generate Random String    50    chars=~`!@#$%^&*()][;',./\<>?
    ${last name}=    Generate Random String    50    chars=~`!@#$%^&*()][;',./\<>?
    ${status code}    ${test user}=    Create New User    ${first name}    ${last name}
    Status Code Should Be    ${status code}    200
    User Should Exist    ${test user.id}
    [Teardown]    Run Keyword And Ignore Error    Delete User    ${test user}

Create Long User
    [Setup]
    ${first name}=    Generate Random String    150    [LETTERS]
    ${last name}=    Generate Random String    150    [LETTERS]
    ${status code}    ${test user}=    Create New User    ${first name}    ${last name}
    Status Code Should Be    ${status code}    200
    User Should Exist    ${test user.id}
    [Teardown]    Run Keyword And Ignore Error    Delete User    ${test user}

Update Existing User. First Name
    ${first name}=    Generate Random String    10    [LETTERS]
    ${status code}    ${_}=    Update User    ${test user}    first_name=${first name}
    Status Code Should Be    ${status code}    200
    ${status code}    ${updated user 1}=    Find User    ${test user.id}
    Status Code Should Be    ${status code}    200
    Should Be Equal    ${test user.last_name}    ${updated user 1.last_name}
    Should Not Be Equal    ${test user.first_name}    ${updated user 1.first_name}
    Should Be Equal    ${first name}    ${updated user 1.first_name}

Update Existing User. Last Name
    ${last name}=    Generate Random String    10    [LETTERS]
    ${status code}    ${_}=    Update User    ${test user}    last_name=${last name}
    Status Code Should Be    ${status code}    200
    ${status code}    ${updated user 1}=    Find User    ${test user.id}
    Status Code Should Be    ${status code}    200
    Should Be Equal    ${test user.first_name}    ${updated user 1.first_name}
    Should Not Be Equal    ${test user.last_name}    ${updated user 1.last_name}
    Should Be Equal    ${last name}    ${updated user 1.last_name}

Update Existing User. Id
    [Documentation]    Test that ID can't be changed
    [Tags]    known_issue
    ${id}=    Generate Random String    5    [NUMBERS]
    ${status code}    ${_}=    Update User    ${test user}    user_id=${id}
    Status Code Should Be    ${status code}    200    # this seems incorrect
    User Should Not Exist    ${id}
    User Should Exist    ${test user.id}

Update Existing User. Anonymous
    ${status code}    ${_}=    Update User    ${test user}    ${EMPTY}    ${EMPTY}
    Status Code Should Be    ${status code}    200
    ${status code}    ${updated user}=    Find User    ${test user.id}
    Status Code Should Be    ${status code}    200
    Should Be Equal    ${updated user.first_name}    ${EMPTY}
    Should Be Equal    ${updated user.last_name}    ${EMPTY}

*** Keywords ***
Create Test User And Remember
    [Documentation]    Creates new user and store it as Test Variable `${test user}`
    ${_}    ${test user}=    Create New User
    Set Test Variable    ${test user}

Remove Test User
    [Documentation]    Removes user defined in `${test user}` Test Variable
    Run Keyword And Ignore Error    Delete User    ${test user}
